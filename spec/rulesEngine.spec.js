let RulesEngine = require('../rulesEngine');

describe("Unit Test for rules Engine", () => {
    let rulesEngine;

    beforeEach(() => {
        rulesEngine = new RulesEngine();
    });

    it('true should equal true', () => {
        expect(true).toBe(true);
    });

    it('should create Rules Engine object', () => {
        expect(rulesEngine).toBeTruthy();
    });

    it('should have parseRule method', () => {
        expect(rulesEngine.parseRule).toBeTruthy();
    });
});

describe('Test for array tree', () => {
    let rulesEngine;
    beforeEach(() => {
        rulesEngine = new RulesEngine();
    });

    it('should only accept string', () => {
        expect(() => rulesEngine.toArrayTree()).toThrowError('Error parsing to array tree. Cannot parse non string.');
    })

    it('should parse single operand to an array', () => {
        let arrayTree = rulesEngine.toArrayTree('[12]');
        expect(arrayTree).toEqual([12]);

        arrayTree = rulesEngine.toArrayTree('["hello"]');
        expect(arrayTree).toEqual(['hello']);

        arrayTree = rulesEngine.toArrayTree('["eq", 100, 100]');
        expect(arrayTree).toEqual(["eq", 100, 100]);
    });
});

describe("Test for map property", () => {
    let rulesEngine;
    beforeEach(() => {
        rulesEngine = new RulesEngine();
    });

    it('should map property for a given string and object', () => {
        var result = rulesEngine.mapProperty('model.name', {
            model: {
                name: 'test'
            }
        });

        expect(result).toBe('test');
    });

    it('should map to array for given string template and object with collection', () => {
        var result = rulesEngine.mapProperty('model.users.name', {
            model: {
                users: [
                    { name: 'john' },
                    { name: 'Jim' },
                    { name: 'Jane' }
                ]
            }
        });

        expect(result).toEqual(['john', 'Jim', 'Jane']);
    });

    it('should map to array for given string template and object with nested collection', () => {
        var result = rulesEngine.mapProperty('model.users.children.age', {
            model: {
                users: [
                    { name: 'John', children: [{ age: 5 }, { age: 10 }] },
                    { name: 'Jim', children: [{ age: 15 }, { age: 11 }] }
                ]
            }
        });
        expect(result).toEqual([5, 10, 15, 11]);
    })
});

describe('Test for parse rule', () => {
    let rulesEngine;
    beforeEach(() => {
        rulesEngine = new RulesEngine();
    });

    it('Should return a method that evaluates expression', () => {
        let eval = rulesEngine.parseRule('[ 11 ]');
        expect(eval).toBeTruthy();
    });

    it('number - should perform null operation (return as it is) if no operation is passed', () => {
        let eval = rulesEngine.parseRule('[ 12 ]');
        let result = eval();
        expect(result).toBe(12);
    });

    it('string - should perform null operation (return as it is) if no operation is passed', () => {
        let eval = rulesEngine.parseRule('[ "hello" ]');
        let result = eval();
        expect(result).toBe('hello');
    });

    it('template - should perform null operation (return mapped value) if no operation is passed', () => {
        let eval = rulesEngine.parseRule('[ "{model.test}" ]');
        let result = eval({
            model: {
                test: 'test'
            }
        });
        expect(result).toBe('test');

        result = eval({
            model: {
                test: 15
            }
        });

        expect(result).toBe(15);

        result = eval({
            model: {
                test: [12, 3, 4]
            }
        });

        expect(result).toEqual([12, 3, 4]);
    });

    it('should add numbers if + operation', () => {
        let eval = rulesEngine.parseRule('["add", 4, 5]');
        result = eval();
        expect(result).toBe(9);
    });

    it('should add array of numbers if + operation', () => {
        let eval = rulesEngine.parseRule('["add", [1, 2, 3]]');
        result = eval();
        expect(result).toBe(6);
    });

    it('should add array of numbers from template if + operation', () => {
        let eval = rulesEngine.parseRule('["add", "{model.users.age}"]');
        let result = eval({
            model: {
                users: [
                    { age: 5 },
                    { age: 6 },
                    { age: 7 }
                ]
            }
        });

        expect(result).toBe(18);
    })

    it('should concatenate strings if join operation', () => {
        let eval = rulesEngine.parseRule('["join", " ", ["Hello,", "World", "!"]]');
        result = eval();
        expect(result).toBe("Hello, World !");
    });

    it('should pass random tests - sum of value (template) == 100', () => {
        let eval = rulesEngine.parseRule('["eq", ["add", "{model.basements.percentage}"], 100]');
        let result = eval({
            model: {
                basements: [
                    { type: 'concrete', percentage: 20 },
                    { type: 'dark', percentage: 30 }
                ]
            }
        });

        expect(result).toBe(false);

        result = eval({
            model: {
                basements: [
                    { type: 'concrete', percentage: 20 },
                    { type: 'dark', percentage: 30 },
                    { type: 'finished', percentage: 50 }
                ]
            }
        });

        expect(result).toBe(true);
    });

    it('should pass random test - if sum of value (template) is less than 100 return sum of the value else return 100', () => {
        let eval = rulesEngine.parseRule(`["ife",
            [ "lt", [ "add", "{model.basements.percentage}"], 100],
            ["add", "{model.basements.percentage}"],
            100
        ]`);

        let result = eval({
            model: {
                basements: [
                    { type: 'concrete', percentage: 20 },
                    { type: 'dark', percentage: 30 }
                ]
            }
        });
        expect(result).toBe(50);

        result = eval({
            model: {
                basements: [
                    { type: 'concrete', percentage: 20 },
                    { type: 'dark', percentage: 30 },
                    { type: 'finished', percentage: 55 }
                ]
            }
        });

        expect(result).toBe(100);
    });

    it('should pass random test - a long operation run multiple times', () => {
        let eval = rulesEngine.parseRule(`["adr", "{model.numbers}"]`);

        for (i = 0; i <= 30; i++) {
            let result = eval({
                model: {
                    numbers: [1, 10000000]
                }
            });

            expect(result).toBe(50000005000000);
        }
    });

    it('should pass random test - a complicated operation', () => {
        rule =
            `["ife",
            ["and",
                ["and",
                    ["gt",
                        ["adr", 1, 10],
                        ["add", "{model.basements.sqft}"]
                    ],
                    ["lte",
                        ["adr", 1, 8],
                        ["add", "{model.basements.sqft}"]
                    ]
                ],
                ["eq",
                    100,
                    ["add", "{model.basements.percentage}"]
                ]
            ],
            "Rules met",
            "Invalid Data"
        ]`;

        let eval = rulesEngine.parseRule(rule);

        let facts = {
            model: {
                basements: [
                    { sqft: 5, percentage: 30 },
                    { sqft: 15, percentage: 30 },
                    { sqft: 25, percentage: 40 },
                ]
            }
        };

        let result = eval(facts);
        expect(result).toBe('Rules met');

        facts = {
            model: {
                basements: [
                    { sqft: 5, percentage: 30 },
                    { sqft: 25, percentage: 30 },
                    { sqft: 25, percentage: 40 },
                ]
            }
        };

        result = eval(facts);
        expect(result).toBe('Invalid Data');
    })
})
