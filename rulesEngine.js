let _ = require('lodash');
let memoize = require('fast-memoize');

class RulesEngine {

    isTemplate(value) {
        return _.isString(value) && value.startsWith('{') && value.endsWith('}');
    }

    mapProperty(propString, fact) {
        return propString.split('.').reduce((prev, curr) => {
            return _.isArray(prev) ? _.flatMap(prev, p => p[curr]) : prev[curr];
        }, fact);
    };

    operate(operation, operands) {
        let method = operations[operation]
        return !!method ? method(operands): [operation, ...operands];
    }

    evaluate (expressionTree, fact) {
        if(this.isTemplate(expressionTree)) {
            return this.mapProperty(expressionTree.substr(1, expressionTree.length - 2), fact);
        }

        if(_.isNumber(expressionTree) || _.isBoolean(expressionTree) || _.isString(expressionTree)) {
            return expressionTree;
        }

        if(_.isArray(expressionTree)) {
            let operation = expressionTree[0];

            if(expressionTree.length == 1) {
                return this.evaluate(expressionTree[0], fact); //null operation
            }

            let operands = _.flattenDeep(expressionTree.slice(1, expressionTree.length).map(exp => this.evaluate(exp, fact)));
            return this.operate(operation, operands);
        }
    }

    parseRule(rule) {
        let arrayTree = [];
        try {
            arrayTree = this.toArrayTree(rule);
        } catch(ex) {
            throw new Error(`Unable to parse to rule tree. ${ex.message}`);
        }

        let evaluationFunction = (fact) => {
            let expressionTree = arrayTree;

            let evaluate = (exp) => this.evaluate(exp, fact);

            return evaluate(expressionTree);
        }

        return memoize(evaluationFunction);
    }

    toArrayTree(rule) {
        if(!_.isString(rule)) {
            throw new Error('Error parsing to array tree. Cannot parse non string.')
        }
        return JSON.parse(rule);
    }
};


const operations = {
    add: (operands) => {
        return operands.reduce((prev, curr) => prev + curr, 0);
    },
    sub: (operands) => {
        return operands[0] - operands[1];
    },
    mul: (operands) => {
        return operands.reduce((prev, curr) => prev * curr, 1);
    },
    div: (operands) => {
        return +operands[0] / +operands[1];
    },
    avg: (operands) => {
        return operands.reduce((prev, curr) => prev + curr, 0)/operands.length;
    },
    eq: (operands) => {
        return operands[0] == operands[1];
    },
    gt: (operands) => {
        return operands[0] > operands[1];
    },
    lt: (operands) => {
        return operands[0] < operands[1];
    },
    gte: (operands) => {
        return operands[0] >= operands[1];
    },
    lte: (operands) => {
        return operands[0] <= operands[1];
    },
    neq: (operands) => {
        return operands[0] != operands[1];
    },
    and: (operands) => {
        return !!operands[0] && !!operands[1];
    },
    or: (operands) => {
        return !!operands[0] || !!operands[1];
    },
    not: (operands) => {
        return !operands[0];
    },
    ife: (operands) => {
        return operands[0] ? operands[1] : operands[2];
    },
    join: (operands) => {
        return _.join(operands.slice(1), operands[0]);
    },
    adr: (operands) => {
        return _.range(operands[0], operands[1] + 1).reduce((prev, curr) => prev + curr, 0);
    }
}

module.exports = RulesEngine;